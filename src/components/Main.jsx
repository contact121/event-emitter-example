import React, { PureComponent } from 'react';
import Home from './Home/Home.jsx';
import Sidebar from './Sidebar/Sidebar.jsx';
import { AppContext } from './AppContext.jsx';
import EventEmitter from '../utils/EventEmitter.js';

const client = new EventEmitter();

export default class Main extends PureComponent {
	appContext = {
		client: client
	};

	render() {
		return (
			<AppContext.Provider value={ this.appContext }>
				<main style={ { display: 'flex', justifyContent: 'center' } }>
					<Home/>
					<Sidebar/>
				</main>
			</AppContext.Provider>
		)
	}
}