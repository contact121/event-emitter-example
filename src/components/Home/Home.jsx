import React, { PureComponent } from 'react';
import randomWords from 'random-words';
import { AppContext } from '../AppContext.jsx';

export default class Home extends PureComponent {
	_onClick = () => {
		const { client } = this.context;
		client.emit('PRESS_BUTTON', { word: randomWords() })
	};

	render() {
		return (
			<section>
				<h1>Home.jsx</h1>
				<button
					onClick={ this._onClick }>
					Click me
				</button>
			</section>
		)
	}
}

Home.contextType = AppContext;