import React, { PureComponent } from 'react';
import { AppContext } from '../AppContext.jsx';

export default class Line extends PureComponent {
	_deleteItem = () => {
		const { index } = this.props;
		const { client } = this.context;
		client.emit('DELETE_ITEM', { index: index });
	};

	render() {
		const { item } = this.props;
		return (
			<li
				style={ { cursor: 'pointer' } }
				onClick={ this._deleteItem }>
				{ item }
			</li>
		)
	}
}

Line.contextType = AppContext;