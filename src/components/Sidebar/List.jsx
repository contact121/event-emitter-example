import React, { PureComponent } from 'react';
import Line from './Line.jsx';

export default class List extends PureComponent {
	_getList = () => {
		const { list } = this.props;
		return list.map((item, i) => (
			<Line
				key={ i }
				index={ i }
				item={ item }/>
		))
	};

	render() {
		return (
			<ul>
				{ this._getList() }
			</ul>
		)
	}
}