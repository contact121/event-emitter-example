import React, { PureComponent } from 'react';
import { AppContext } from '../AppContext.jsx';
import List from './List.jsx';

export default class Sidebar extends PureComponent {
	state = {
		buttonClickedNTimes: 0,
		deletedNItems: 0,
		words: []
	};

	componentDidMount() {
		const { client } = this.context;
		client.on('PRESS_BUTTON', this._handleButtonClick);
		client.on('DELETE_ITEM', this._handleDeleteItem);
	}

	_handleButtonClick = payload => {
		const { buttonClickedNTimes, words } = this.state;
		this.setState({ buttonClickedNTimes: buttonClickedNTimes + 1, words: [...words, payload.word] });
	};

	_handleDeleteItem = payload => {
		const { deletedNItems, words } = this.state;
		this.setState({ deletedNItems: deletedNItems + 1, words: words.filter((w, i) => i !== payload.index) })
	};

	componentWillUnmount() {
		const { client } = this.context;
		client.removeEventListener('PRESS_BUTTON', this._handleButtonClick)
	}

	render() {
		const { buttonClickedNTimes, deletedNItems, words } = this.state;
		return (
			<nav style={ { marginLeft: 50 } }>
				<h1>Sidebar.jsx</h1>
				<p>
					The button has been clicked { buttonClickedNTimes } { buttonClickedNTimes === 1 ? 'time' : 'times' }
				</p>
				<p>
					I have deleted { deletedNItems } { deletedNItems === 1 ? 'item' : 'items' }
				</p>
				<List list={ words }/>
			</nav>
		)
	}
}

Sidebar.contextType = AppContext;